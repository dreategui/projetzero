﻿using System . Collections ;
using System . Collections . Generic ;
using UnityEngine ;
[ RequireComponent ( typeof ( Rigidbody2D ))]
public class MyController : MonoBehaviour {
    private Rigidbody2D rb2d = null ;
    private Animator anim;
    private float move = 0f;
    private float attacking = 0f;
    private float jumping = 0f;
    private bool flipped = false;
    private bool grounded = true;
    private bool muerto = false;
    public float maxS = 100f;
    // Use this for initialization
    void Awake () {
        rb2d = GetComponent < Rigidbody2D >();
        anim = GetComponent < Animator >();
    }
    public void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "muerte"){
            muerto = true;
            anim.SetBool("dead", true);
        }
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (muerto){
            return;
        }
        //primero checkeo de inputs
        move = Input .GetAxis("Horizontal");
        attacking = Input.GetAxis("Attack");
        jumping = Input.GetAxis("Vertical");
        
        if (rb2d.velocity.y > 0.001f || rb2d.velocity.y < -0.001f){//si la velocidad en y es diferente de 0 significa que esta saltando asi que no puede volver a saltar
            grounded = false;
        }else{
            grounded = true;
        }
        if (attacking != 0f){//si ataca se bloquea el movimiento y se cambia la animacion a atacar
            move = 0f;
            anim.SetBool("attack", true);
            anim.SetBool("run", false);
        }else{
            anim.SetBool("attack", false);
        }
        if (jumping != 0f && grounded){
            move = 0f;
            anim.SetBool("jump", true);
            rb2d.velocity = new Vector2(jumping*maxS*Time.fixedDeltaTime, 30);
        }else{
            anim.SetBool("jump", false);
        }
        rb2d.velocity = new Vector2(move*2*maxS*Time.fixedDeltaTime, rb2d.velocity.y);
        if (rb2d.velocity.x > 0.001f || rb2d.velocity.x < - 0.001f ) {
            if ((rb2d.velocity.x < - 0.001f && !flipped) || (rb2d.velocity.x > - 0.001f && flipped)) {
                flipped = !flipped;
                this.transform.rotation = Quaternion.Euler(0,flipped?180:0, 0);
            }
            anim.SetBool("run", true);
            }else{
                anim.SetBool("run", false);
        }
    }
}